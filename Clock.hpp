// Copyright � 2023 Acadia Softworks LLC. All rights reserved.

#ifndef CLOCK_HPP
#define CLOCK_HPP

#include <chrono>

class Clock {
public:
	Clock();
	~Clock();

	void Start();
	void Update();
	void Stop();
	void Reset();

	double GetElapsedTime();
	double GetDeltaTime();
	
private:
	
	std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
	std::chrono::time_point<std::chrono::high_resolution_clock> end_time;
	std::chrono::time_point<std::chrono::high_resolution_clock> last_time;
	std::chrono::time_point<std::chrono::high_resolution_clock> current_time;

	double elapsed_time;
	double delta_time;
};


#endif // !CLOCK_H

