# Renderer

A graphics rendering library.

Currently only supports OpenGL

## Overview
The purpose of this library is to provide a high-level abstraction for working with multiple graphics APIs while also providing a common set of features for each.


## Usage

The renderer is intended to be a standalone static library.

The primary way to create a render instance is using the provided factory method:

```cpp
Renderer* myrenderer = RendererAPIFactory::CreateRenderer(RenderAPIType::OpenGL, myhwnd);
```
This creates a new instance of the desired renderer in the specified window.

Next, in your main application loop you you'd use the following to perform a render cycle:

```cpp
myrenderer->Clear();
myrenderer->Begin();
myrenderer->End();
myrenderer->Present();
```

Between the `Begin` and `End` methods is where actual drawing would be performed.
