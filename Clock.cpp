#include "Clock.hpp"

void Clock::Start()
{
	start_time = std::chrono::high_resolution_clock::now();
	last_time = start_time;
}

void Clock::Update()
{
	current_time = std::chrono::high_resolution_clock::now();
	delta_time = std::chrono::duration_cast<std::chrono::duration<double>>(current_time - last_time).count();
	last_time = current_time;
}

void Clock::Stop()
{
	end_time = std::chrono::high_resolution_clock::now();
	elapsed_time = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time).count();
}

void Clock::Reset()
{
	start_time = std::chrono::high_resolution_clock::now();
	last_time = start_time;
}

double Clock::GetElapsedTime()
{
	return elapsed_time;
}

double Clock::GetDeltaTime()
{
	return delta_time;
}