

#ifndef  RENDERER_HPP
#define RENDERER_HPP

#include <vector>
#include <Windows.h>
#include <windowsx.h>
#include <wrl.h>
#include "Color.h"
#include "GFXMath.h"



enum class RendererInstanceType {
	RendererInstanceType_Instance,
	RendererInstanceType_Device
	
};

enum class RendererSurfaceType {
	RendererSurfaceType_Window,
	RendererSurfaceType_Surface
};

/// <summary>
/// The Renderer control level for the renderer.
/// </summary>
enum class RendererControlLevel {
	Automatic,
	Manual,
};

enum class RenderAPIType { DirectX9, DirectX11, DirectX12, OpenGL, Vulkan };

/// <summary>
/// The data of the renderer.
/// </summary>
struct RenderContext {
	RenderAPIType RendererAPI;
	RendererInstanceType RendererInstance;
	RendererSurfaceType RendererSurface;
	RendererControlLevel RendererControlLevel;
	HWND WindowHandle;
};

/// <summary>
/// This is the the main renderer API, it's a pure virtual class that all the renderers will inherit from.
/// </summary>
class Renderer {
public:
	Renderer();
	virtual ~Renderer() {}
	/// <summary>
	/// The start of every frame.
	/// </summary>
	virtual void Begin() = 0;
	virtual void Update() = 0;
	virtual void Resize(int width, int height) = 0;
	virtual void SetViewMatrix(Matrix4x4& viewMatrix) = 0;
	/// <summary>
	/// Clears the current frame.
	/// </summary>
	virtual void Clear() = 0;
	virtual void DrawLine() = 0;
	virtual void Draw() = 0;
	virtual void Translate(Vector3 translation) = 0;
	/// <summary>
	/// End of frame.
	/// </summary>
	virtual void End() = 0;
	/// <summary>
	/// Presents the frame to the current surface.
	/// </summary>
	virtual void Present() = 0;
	virtual void RenderText() = 0;

	// make current
	virtual void MakeCurrent() = 0;

	/// <summary>
	/// Stops the renderer.
	/// </summary>
	virtual void Shutdown() = 0;
	
	/// <summary>
	/// Collection of all the renderers.
	/// </summary>
	std::vector<Renderer*> renderers;

private:
	Matrix4x4 m_viewMatrix;
	Matrix4x4 m_projectionMatrix;
	Matrix4x4 m_viewProjectionMatrix;
};

#endif // ! RENDERER_HPP

