
#ifndef COLOR_H
#define COLOR_H

#include <string>
#include <sstream>
#include <iomanip>

/// <summary>
/// A color in normalized RGBA space.
/// </summary>
struct Color {

    // color
    float r, g, b, a;

    Color(float vr, float vg, float vb, float va) {
        r = vr;
        g = vg;
        b = vb;
        a = va;
    }

    Color() {
        r = 0.0f;
        g = 0.0f;
        b = 0.0f;
        a = 0.0f;
    }

    // operator overloads
    Color operator+(const Color& other) {
        return Color(r + other.r, g + other.g, b + other.b, a + other.a);
    }

    Color operator-(const Color& other) {
        return Color(r - other.r, g - other.g, b - other.b, a - other.a);
    }

    // multiplication overload
    Color operator*(const float& scalar) {
        return Color(r * scalar, g * scalar, b * scalar, a * scalar);
    }

    // division overload
    Color operator/(const float& scalar) {
        return Color(r / scalar, g / scalar, b / scalar, a / scalar);
    }

    // equality overload
    bool operator==(const Color& other) {
        return r == other.r && g == other.g && b == other.b && a == other.a;
    }

    // inequality overload
    bool operator!=(const Color& other) {
        return r != other.r || g != other.g || b != other.b || a != other.a;
    }
    
    /// <summary>
    /// Converts the color to standard RGBA values.
    /// </summary>
    /// <returns></returns>
    Color RGBA() {
		r * 255.0f;
		g * 255.0f;
		b * 255.0f;
		a * 255.0f;
		return Color(r, g, b, a);
	}

    /// <summary>
    /// Normalizes the color channels to a value between 0 and 1.
    /// </summary>
    /// <returns> Returns a normalized color </returns>
    Color Normalize() {
		return Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
	}

    /// <summary>
    /// Converts the color to a hex string.
    /// </summary>
    /// <returns> Returns a string value representing a hex code </returns>
    std::string ToHex() const {
        int red = static_cast<int>(r * 255.0f);
        int green = static_cast<int>(g * 255.0f);
        int blue = static_cast<int>(b * 255.0f);
        int alpha = static_cast<int>(a * 255.0f);

        std::stringstream ss;
        ss << "#" << std::setfill('0') << std::hex
            << std::setw(2) << red
            << std::setw(2) << green
            << std::setw(2) << blue
            << std::setw(2) << alpha;

        return ss.str();
    }

    // HexToRGB
    Color HexToRGB(std::string hex) {
		hex.erase(0, 1);
		int red = std::stoi(hex.substr(0, 2), nullptr, 16);
		int green = std::stoi(hex.substr(2, 2), nullptr, 16);
		int blue = std::stoi(hex.substr(4, 2), nullptr, 16);
		return Color(red / 255.0f, green / 255.0f, blue / 255.0f, 1.0f);
	}

    /// <summary>
    /// Creates the compliment of the color.
    /// </summary>
    /// <returns></returns>
    Color Compliment() {
		return Color(1.0f - r, 1.0f - g, 1.0f - b, 1.0f - a);
	}

    // gradient
    Color Gradient(Color& other, float t) {
        float r = this->r + (other.r - this->r) * t;
    }

    /// <summary>
    /// Blends two colors together using a given weight.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="weight"></param>
    /// <returns>Returns a blended color</returns>
    Color Blend(const Color& color, float weight) const {
        float r = (1.0f - weight) * this->r + weight * color.r;
        float g = (1.0f - weight) * this->g + weight * color.g;
        float b = (1.0f - weight) * this->b + weight * color.b;
        float a = (1.0f - weight) * this->a + weight * color.a;

        return Color(r, g, b, a);
    } 


#define ACADAIA_TRANSPARENT Color(0.0f, 0.0f, 0.0f, 0.0f)
#define WHITE Color(1.0f, 1.0f, 1.0f, 1.0f)
#define BLACK Color(0.0f, 0.0f, 0.0f, 1.0f)
#define RED Color(1.0f, 0.0f, 0.0f, 1.0f)
#define GREEN Color(0.0f, 1.0f, 0.0f, 1.0f)
#define BLUE Color(0.0f, 0.0f, 1.0f, 1.0f)
#define YELLOW Color(1.0f, 1.0f, 0.0f, 1.0f)
#define MAGENTA Color(1.0f, 0.0f, 1.0f, 1.0f)
#define CYAN Color(0.0f, 1.0f, 1.0f, 1.0f)
#define ORANGE Color(1.0f, 0.5f, 0.0f, 1.0f)
#define PURPLE Color(0.5f, 0.0f, 0.5f, 1.0f)
#define PINK Color(1.0f, 0.0f, 0.5f, 1.0f)
#define GREY Color(0.5f, 0.5f, 0.5f, 1.0f)
#define BROWN Color(0.5f, 0.25f, 0.0f, 1.0f)
#define MAROON Color(0.5f, 0.0f, 0.0f, 1.0f)
#define NAVY Color(0.0f, 0.0f, 0.5f, 1.0f)
#define TEAL Color(0.0f, 0.5f, 0.5f, 1.0f)
#define FOREST Color(0.0f, 0.5f, 0.0f, 1.0f)
#define SKY Color(0.0f, 0.75f, 1.0f, 1.0f)
#define TWILIGHT Color(0.5f, 0.0f, 0.5f, 1.0f)
#define DAWN Color(0.5f, 0.5f, 0.0f, 1.0f)
#define FIREFLY Color(0.0f, 0.5f, 0.25f, 1.0f)
#define CERULEAN Color(0.0f, 0.5f, 1.0f, 1.0f)
#define ACADIA Color(0.0f, 0.0f, 0.5f, 1.0f)
#define VIOLET Color(0.5f, 0.0f, 0.5f, 1.0f)
#define LILAC Color(0.5f, 0.0f, 0.75f, 1.0f)
#define LAVENDER Color(0.5f, 0.0f, 1.0f, 1.0f)
#define BURGANDY Color(0.5f, 0.0f, 0.0f, 1.0f)

};

#endif
