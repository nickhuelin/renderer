
#ifndef RENDERER_OPEN_GL
#define RENDERER_OPEN_GL

#include "glad/include/glad/glad.h"
#include "glad/include/glad/glad_wgl.h"
#include "Renderer.hpp"
#include "GLSLShader.h"


/// <summary>
/// This class is responsible for managing the OpenGL graphics API.
/// </summary>
class RendererOpenGL : public Renderer {

public:
	RendererOpenGL(HWND window);
	~RendererOpenGL();
	void Begin() override;
	void Update() override {}
	void Resize(int width, int height) override;
	void Clear() override;
	void Draw() override;
	void SetViewMatrix(Matrix4x4& viewMatrix) override;
	void DrawLine() override {}
	void Translate(Vector3 translation) override;
	void End() override;
	void Present() override;
	void RenderText() override {}
	void Shutdown() override;

	// make current
	void MakeCurrent() override;

private:
	HWND m_window;
	HDC hdc;
	HGLRC hrc;
	PIXELFORMATDESCRIPTOR pfd;
};

#endif // !RENDERER_OPEN_GL

