#ifndef GFX_MATH_X
#define GFX_MATH_X

/// <summary>
/// A 2-dimensional vector in cartesian space.
/// </summary>
struct Vector2 {
	float x, y;
	inline Vector2(float vx, float vy) {
		x = vx;
		y = vy;
	}

	inline Vector2() {
		x = 0.0f;
		y = 0.0f;
	}
	/// <summary>
	/// Returns the length of the vector.
	/// </summary>
	inline float Length() {
		return sqrt(x * x + y * y);
	}

	// + operator overload
	inline Vector2 operator+(const Vector2& other) {
		return Vector2(x + other.x, y + other.y);
	}

	// & operator overload
	inline Vector2 operator-(const Vector2& other) {
		return Vector2(x - other.x, y - other.y);
	}

	// * operator overload
	inline Vector2 operator*(const float& scalar) {
		return Vector2(x * scalar, y * scalar);
	}

	// / operator overload
	inline Vector2 operator/(const float& scalar) {
		return Vector2(x / scalar, y / scalar);
	}

	// == operator overload
	inline bool operator==(const Vector2& other) {
		return x == other.x && y == other.y;
	}

	// != operator overload
	inline bool operator!=(const Vector2& other) {
		return x != other.x || y != other.y;
	}

	// += operator overload
	inline Vector2 operator+=(const Vector2& other) {
		x += other.x;
		y += other.y;
		return *this;
	}

	/// <summary>
	/// Retrieves the midpoint of the vector.
	/// </summary>
	inline Vector2 Midpoint() {
		return Vector2(x / 2.0f, y / 2.0f);
	}

	inline float Hypotenuse(float a, float b) {
		return sqrtf(powf(a, 2.0f) + powf(b, 2.0f));
	}

	// magnitude
	inline float Magnitude() {
		return sqrtf(powf(x, 2.0f) + powf(y, 2.0f));
	}

	/// <summary>
	/// Returns the unit vector, with all values normalized to 1
	/// </summary>
	/// <returns>Vector2</returns>
	inline Vector2 Normalize() {
		return Vector2(x / Magnitude(), y / Magnitude());
	}

	/// <summary>
	/// Returns the dot product of two vectors
	/// </summary>
	/// <param name="other">The other vector in the equation</param>
	/// <returns>Vector2</returns>
	inline float Dot(Vector2 other) {
		return x * other.x + y * other.y;
	}


	/// <summary>
	/// Returns the distance between two points.
	///	</summary>	
	inline float Distance(Vector2 other) {
		return  sqrtf(powf(x - other.x, 2.0f) + powf(y - other.y, 2.0f));
	}

};

// smoothstep
inline float Smoothstep(float edge0, float edge1, float x) {
	x = fmaxf(0.0f, fminf(1.0f, (x - edge0) / (edge1 - edge0)));
	return x * x * (3 - 2 * x);
}

/// <summary>
/// A 3-dimensional vector in cartesian space.
/// </summary>
struct Vector3 {
	float x, y, z;
	inline Vector3(float vx, float vy, float vz) {
		x = vx;
		y = vy;
		z = vz;
	}

	inline Vector3() {
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}

	// operator overloads
	inline Vector3 operator+(const Vector3& other) {
		return Vector3(x + other.x, y + other.y, z + other.z);
	}

	inline Vector3 operator-(const Vector3& other) {
		return Vector3(x - other.x, y - other.y, z - other.z);
	}

	// multiplication overload
	inline Vector3 operator*(const float& scalar) {
		return Vector3(x * scalar, y * scalar, z * scalar);
	}

	// const vector3 -
	inline Vector3 operator-() const {
		return Vector3(-x, -y, -z);
	}

	// const Vector3- overload
	inline const Vector3 operator-(const Vector3& other) const {
		return Vector3(x - other.x, y - other.y, z - other.z);
	}

	// division overload
	inline Vector3 operator/(const float& scalar) {
		return Vector3(x / scalar, y / scalar, z / scalar);
	}

	// equality overload
	inline bool operator==(const Vector3& other) {
		return x == other.x && y == other.y && z == other.z;
	}

	// inequality overload
	inline bool operator!=(const Vector3& other) {
		return x != other.x || y != other.y || z != other.z;
	}


	// dot product
	inline float Dot(const Vector3& other) {
		return x * other.x + y * other.y + z * other.z;
	}

	// cross product
	inline Vector3 Cross(const Vector3& other) {
		return Vector3(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
	}

	// cross product with const vector
	inline const Vector3 Cross(const Vector3& other) const {
		return Vector3(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
	}

	// normalize
	inline Vector3 Normalize() {
		return Vector3(x / Magnitude(), y / Magnitude(), z / Magnitude());
	}

	// normalize with const vector
	inline const Vector3 Normalize() const {
		return Vector3(x / Magnitude(), y / Magnitude(), z / Magnitude());
	}

	inline float Hypotenuse(float a, float b, float c) {
		return sqrtf(powf(a, 2.0f) + powf(b, 2.0f) + powf(c, 2.0f));
	}

	// magnitude
	inline const float Magnitude() {
		return sqrtf(powf(x, 2.0f) + powf(y, 2.0f) + powf(z, 2.0f));
	}

	// magnitude with const vector
	inline const float Magnitude() const {
		return sqrtf(powf(x, 2.0f) + powf(y, 2.0f) + powf(z, 2.0f));
	}

	// up
	static Vector3 Up() {
		return Vector3(0.0f, 1.0f, 0.0f);
	}
};

struct Vector4 {
	float x, y, z, w;
	inline Vector4(float vx, float vy, float vz, float vw) {
		x = vx;
		y = vy;
		z = vz;
		w = vw;
	}

	inline Vector4() {
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
		w = 0.0f;
	}

	// operator overloads
	inline Vector4 operator+(const Vector4& other) const {
		return Vector4(x + other.x, y + other.y, z + other.z, w + other.w);
	}

	inline Vector4 operator-(const Vector4& other) const {
		return Vector4(x - other.x, y - other.y, z - other.z, w - other.w);
	}
};

/// <summary>
/// Converts radians to degrees.
/// </summary>
/// <param name="radians"></param>
/// <returns></returns>
static float RadiansToDegrees(float radians) {
	return radians * (180.0f / 3.14159265358979323846);
}

/// <summary>
/// Converts degrees to radians.
/// </summary>
/// <param name="degrees"></param>
/// <returns></returns>
static inline float DegreesToRadians(float degrees) {
	return degrees * (3.14159265358979323846 / 180.0f);
}

/// <summary>
/// Matrix3x3 struct representing a 3x3 matrix, used for 2D transformations.
/// </summary>
struct Matrix3x3
{
	float elements[3][3];

	Matrix3x3()
	{
		memset(elements, 0, sizeof(elements));
	}

	inline Matrix3x3(float m00, float m01, float m02,
		float m10, float m11, float m12,
		float m20, float m21, float m22)
	{
		elements[0][0] = m00; elements[0][1] = m01; elements[0][2] = m02;
		elements[1][0] = m10; elements[1][1] = m11; elements[1][2] = m12;
		elements[2][0] = m20; elements[2][1] = m21; elements[2][2] = m22;
	}
};

/// <summary>
/// Matrix4x4 struct representing a 4x4 matrix, used for 3D transformations.
/// </summary>
struct Matrix4x4 {
	float elements[4][4];

	Matrix4x4()
	{
		memset(elements, 0, sizeof(elements));
	}

	inline void SetIdentity() {
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				if (i == j) {
					elements[i][j] = 1.0f;
				}
				else {
					elements[i][j] = 0.0f;
				}
			}
		}
	}

	// assignment operator overload
	inline Matrix4x4& operator=(const Matrix4x4& other) {
		memcpy(elements, other.elements, sizeof(elements));
		return *this;
	}

	// vector assignment operator overload
	inline Matrix4x4& operator=(const Vector3& other) {
		elements[0][0] = other.x;
		elements[1][1] = other.y;
		elements[2][2] = other.z;
		elements[3][3] = 1.0f;
		return *this;
	}

	// multiply operator overload
	inline Matrix4x4 operator*(const Matrix4x4& other) const {
		Matrix4x4 result;

		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				result.elements[i][j] = 0.0f;
				for (int k = 0; k < 4; ++k) {
					result.elements[i][j] += elements[i][k] * other.elements[k][j];
				}
			}
		}

		return result;
	}

	inline Matrix4x4(float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33)
	{
		elements[0][0] = m00; elements[0][1] = m01; elements[0][2] = m02; elements[0][3] = m03;
		elements[1][0] = m10; elements[1][1] = m11; elements[1][2] = m12; elements[1][3] = m13;
		elements[2][0] = m20; elements[2][1] = m21; elements[2][2] = m22; elements[2][3] = m23;
		elements[3][0] = m30; elements[3][1] = m31; elements[3][2] = m32; elements[3][3] = m33;
	}

	// Method to rotate around the X axis
	inline void RotateX(float angleDegrees) {
		float angleRadians = DegreesToRadians(angleDegrees);
		float cosA = cos(angleRadians);
		float sinA = sin(angleRadians);

		Matrix4x4 rotationMatrix(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, cosA, -sinA, 0.0f,
			0.0f, sinA, cosA, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);

		*this = (*this) * rotationMatrix;
	}

	// Method to rotate around the Y axis
	inline void RotateY(float angleDegrees) {
		float angleRadians = DegreesToRadians(angleDegrees);
		float cosA = cos(angleRadians);
		float sinA = sin(angleRadians);

		Matrix4x4 rotationMatrix(cosA, 0.0f, sinA, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			-sinA, 0.0f, cosA, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);

		*this = (*this) * rotationMatrix;
	}

	// Method to rotate around the Z axis
	inline void RotateZ(float angleDegrees) {
		float angleRadians = DegreesToRadians(angleDegrees);
		float cosA = cos(angleRadians);
		float sinA = sin(angleRadians);

		Matrix4x4 rotationMatrix(cosA, -sinA, 0.0f, 0.0f,
			sinA, cosA, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);

		*this = (*this) * rotationMatrix;
	}

	

	


	// translation
	inline void Translate(const Vector3& translation) {
		elements[0][3] += translation.x;
		elements[1][3] += translation.y;
		elements[2][3] += translation.z;
	}

	// scale
	inline void Scale(const Vector3& scale) {
		elements[0][0] *= scale.x;
		elements[1][1] *= scale.y;
		elements[2][2] *= scale.z;
	}

	inline void ExtractData(float* target) const {
		int index = 0;
		for (int col = 0; col < 4; col++) {
			for (int row = 0; row < 4; row++) {
				target[index] = elements[row][col];
				index++;
			}
		}
	}

	// perspective
	inline void Perspective(float fov, float aspectRatio, float m_near, float m_far) {
		float fovRadians = DegreesToRadians(fov);
		float tanHalfFov = tanf(fovRadians / 2.0f);

		// Reset all matrix elements to 0
		memset(elements, 0, sizeof(elements));

		elements[0][0] = 1.0f / (aspectRatio * tanHalfFov);
		elements[1][1] = 1.0f / tanHalfFov;
		elements[2][2] = -(m_far + m_near) / (m_far - m_near);
		elements[2][3] = -(2.0f * m_far * m_near) / (m_far - m_near);
		elements[3][2] = -1.0f;
		elements[3][3] = 0.0f;
	}


	// look at
	inline void LookAt(const Vector3& eye, const Vector3& target, const Vector3& up) {
		Vector3 zAxis = (eye - target).Normalize();
		Vector3 xAxis = (up.Cross(zAxis)).Normalize();
		Vector3 yAxis = zAxis.Cross(xAxis);

		elements[0][0] = xAxis.x;
		elements[0][1] = xAxis.y;
		elements[0][2] = xAxis.z;
		elements[0][3] = -xAxis.Dot(eye);

		elements[1][0] = yAxis.x;
		elements[1][1] = yAxis.y;
		elements[1][2] = yAxis.z;
		elements[1][3] = -yAxis.Dot(eye);

		elements[2][0] = zAxis.x;
		elements[2][1] = zAxis.y;
		elements[2][2] = zAxis.z;
		elements[2][3] = -zAxis.Dot(eye);

		elements[3][0] = 0.0f;
		elements[3][1] = 0.0f;
		elements[3][2] = 0.0f;
		elements[3][3] = 1.0f;
	}

	// orthographic
	inline void Orthographic(float left, float right, float bottom, float top, float m_near, float m_far) {
		// Reset all matrix elements to 0
		memset(elements, 0, sizeof(elements));

		elements[0][0] = 2.0f / (right - left);
		elements[1][1] = 2.0f / (top - bottom);
		elements[2][2] = -2.0f / (m_far - m_near);
		elements[3][3] = 1.0f;
		elements[0][3] = -(right + left) / (right - left);
		elements[1][3] = -(top + bottom) / (top - bottom);
		elements[2][3] = -(m_far + m_near) / (m_far - m_near);
	}

};

#endif // !MATH_H
