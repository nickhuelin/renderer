#include "RenderDX12.h"
#include "RendererDX12.h"

RendererDX12::RendererDX12()
{
	CreateDevice();
}

RendererDX12::~RendererDX12()
{


}

void RendererDX12::Begin()
{
	

}

void RendererDX12::Update()
{

}

void RendererDX12::Resize(int width, int height)
{

}

void RendererDX12::Clear()
{

}

void RendererDX12::Draw()
{

}

void RendererDX12::DrawLine()
{

}

void RendererDX12::SetViewMatrix(Matrix4x4& viewMatrix)
{

}

void RendererDX12::End()
{

}

void RendererDX12::Present()
{

}

void RendererDX12::Translate(Vector3 translation)
{

}

void RendererDX12::RenderText()
{

}

void RendererDX12::Shutdown()
{

	
}

void RendererDX12::MakeCurrent()
{
}

void RendererDX12::CreateDevice() {


}

