#include "RendererOpenGL.hpp"

GLuint VAO, VBO, EBO;
GLuint indices_size;
GLint MVPShaderPtr;
GLSLShader Shader;
Matrix4x4 mvp = Matrix4x4();

RendererOpenGL::RendererOpenGL(HWND window)
{
	m_window = window;
	// Create OpenGL context
	hdc = GetDC(window);
	// pixel format descriptor
	pfd = {
	sizeof(PIXELFORMATDESCRIPTOR),   // size of this pfd  
	1,                     // version number  
	PFD_DRAW_TO_WINDOW |   // support window 
	PFD_SUPPORT_OPENGL |   // support OpenGL  
	PFD_DOUBLEBUFFER,      // double buffered
	PFD_TYPE_RGBA,         // RGBA type  
	32,                    // 32-bit color depth  
	0, 0, 0, 0, 0, 0,      // color bits ignored  
	0,                     // no alpha buffer  
	0,                     // shift bit ignored  
	0,                     // no accumulation buffer  
	0, 0, 0, 0,            // accum bits ignored  
	32,                    // 32-bit z-buffer  
	0,                     // no stencil buffer  
	0,                     // no auxiliary buffer  
	PFD_MAIN_PLANE,        // main layer  
	0,                     // reserved  
	0, 0, 0                // layer masks ignored  
	};
	int pixelFormat = ChoosePixelFormat(hdc, &pfd);
	SetPixelFormat(hdc, pixelFormat, &pfd);

	
	renderers.push_back(this);

	// get supported OpenGL version
	hrc = wglCreateContext(hdc);
	if (!hrc) {
		printf("Failed to create OpenGL context\n");
	}

	if (!wglMakeCurrent(hdc, hrc)) {
		printf("Failed to make current\n");
	}

	// Load OpenGL functions
	if (!gladLoadGL()) {
		printf("Failed to load OpenGL\n");
	}

	gladLoadWGL(hdc);

	// disable vsync
	wglSwapIntervalEXT(0);

	// get OpenGL version
	const GLubyte* version = glGetString(GL_VERSION);


	unsigned int indices[] = {
		0, 3, 1,
		1, 3, 2,
		2, 3, 0,
		0, 1, 2
	};

	// cube vertices
	GLfloat vertices[] = {
		// draw single triangle
		-0.5f, -0.5f, 0.0f, // left
		0.5f, -0.5f, 0.0f, // right
		0.0f, 0.5f, 0.0f // top

	};

	
	

	glGenVertexArrays(1, &VAO);
	// bind vertex array object
	glBindVertexArray(VAO);

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	// load shader from file path
	// Shader.LoadFromFile();

	// compile shader  
	Shader.CreateAndLinkProgram();
	
	// activate shader program
	Shader.Activate();
	SetViewMatrix(mvp);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

RendererOpenGL::~RendererOpenGL()
{
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hrc);

	ReleaseDC(m_window, hdc);
}

void RendererOpenGL::Begin()
{

	mvp.RotateY(0.01f);
	// matrix
	glUniformMatrix4fv(MVPShaderPtr, 1, GL_TRUE, &mvp.elements[0][0]); // column major order
	
	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
}

void RendererOpenGL::Resize(int width, int height) 
{
	glViewport(0, 0, width, height);
	glScissor(0, 0, width, height);
}

void RendererOpenGL::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// clear color to gray-blue
	glClearColor(0.18f, 0.28f, 0.43f, 1.0f);
	
}

void RendererOpenGL::Draw()
{

}

void RendererOpenGL::SetViewMatrix(Matrix4x4& viewMatrix)
{
	// model
	Matrix4x4 model = Matrix4x4();
	model.SetIdentity();

	// set up perspective matrix
	Matrix4x4 projection = Matrix4x4();
	projection.SetIdentity();

	projection.Orthographic(-1.0f, 1.0f, -1.0f, 1.0f, 0.001f, 5.0f);

	// view
	Matrix4x4 view = Matrix4x4();
	view.SetIdentity();

	view.LookAt(Vector3(0.0f, 0.0f, -3.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));

	mvp.SetIdentity();

	// model view projection matrix
	mvp = projection * view * model;


	MVPShaderPtr = glGetUniformLocation(Shader.GetProgram(), "mvp");
	// set uniform pointer to model matrix
	glUniformMatrix4fv(MVPShaderPtr, 1, GL_TRUE, &mvp.elements[0][0]); // column major order
}

void RendererOpenGL::Translate(Vector3 translation)
{
	mvp.Translate(translation);
}

void RendererOpenGL::End()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RendererOpenGL::Present()
{
	SwapBuffers(hdc);
}

void RendererOpenGL::Shutdown()
{
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hrc);
	ReleaseDC(m_window, hdc);

	// release all OpenGL memory
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
	
	Shader.Delete();

	// remove renderer from list
	for (int i = 0; i < renderers.size(); i++) {
		if (renderers[i] == this) {
			renderers.erase(renderers.begin() + i);
			break;
		}
	}

	delete this;
	
}

// make current
void RendererOpenGL::MakeCurrent()
{
	wglMakeCurrent(hdc, hrc);
}
