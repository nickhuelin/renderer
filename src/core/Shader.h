#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader {

public:
	Shader();
	Shader(const char* vertexPath, const char* fragmentPath);
	~Shader();

	virtual void Activate() = 0;
	virtual void Deactivate() = 0;
	virtual void Delete() = 0;

	virtual void LoadFromFile(const char* vertexPath, const char* fragmentPath ) = 0;

private:

	unsigned int ID = 0;
};

#endif
