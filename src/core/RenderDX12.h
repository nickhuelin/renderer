
#ifndef RENDERER_FACTORY_DX12
#define RENDERER_FACTORY_DX12



#include <d3d12.h>
#include <dxgi1_6.h>
#include "Renderer.hpp"
#include "Logger.hpp"

using namespace Microsoft::WRL;

class RendererDX12 : public Renderer {
public:
	RendererDX12();
	~RendererDX12();

	void Begin() override;
	void Update() override;
	void Resize(int width, int height) override;
	void Clear() override;
	void Draw() override;
	void DrawLine() override;
	void SetViewMatrix(Matrix4x4& viewMatrix) override;
	void End() override;
	void Present() override;
	void Translate(Vector3 translation) override;
	void RenderText() override;
	void Shutdown() override;

	void MakeCurrent() override;


private:
	// factory
	IDXGIFactory4* m_factory;
	ComPtr<ID3D12Device> m_device;
	ComPtr<ID3D12CommandQueue> m_commandQueue;
	ID3D12CommandAllocator* m_commandAllocator;
	ID3D12GraphicsCommandList* m_commandList;
	ID3D12DescriptorHeap* m_descriptorHeap;
	ID3D12RootSignature* m_rootSignature;
	ID3D12PipelineState* m_pipelineState;
	// comptr fence
	ComPtr<ID3D12Fence> m_fence;

	HANDLE m_fenceEvent;
	UINT64 m_fenceValue;
	// swap chain
	IDXGISwapChain1 *m_swapChain;

	// window handle
	HWND m_window;

	IDXGIAdapter4* GetHardwareAdapter();

	ComPtr <ID3D12Device> GetDevice() const { return m_device; }
	ComPtr <ID3D12CommandQueue> GetCommandQueue() const { return m_commandQueue; }
	ID3D12CommandAllocator* GetCommandAllocator() const { return m_commandAllocator; }
	ID3D12GraphicsCommandList* GetCommandList() const { return m_commandList; }
	ID3D12DescriptorHeap* GetDescriptorHeap() const { return m_descriptorHeap; }
	ID3D12RootSignature* GetRootSignature() const { return m_rootSignature; }
	ID3D12PipelineState* GetPipelineState() const { return m_pipelineState; }
	ComPtr<ID3D12Fence> GetFence() const { return m_fence; }
	HANDLE GetFenceEvent() const { return m_fenceEvent; }

	UINT descriptorSize;

	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS m4xMsaaQuality;

	void CreateDevice();
	void CreateCommandQueue();
	void CreateCommandAllocator();
	void CreateCommandList();
	void CreateDescriptorHeap();
	void CreateRootSignature();
	void CreatePipelineState();
	void CreateFence();
	void CreateFenceEvent();
	void WaitForPreviousFrame();

};

#endif

