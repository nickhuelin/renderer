#version 460 core

layout(location = 0) in vec3 position;

uniform mat4 mvp; // Use the same variable name as in your C++ code

void main()
{
    gl_Position = mvp * vec4(position, 1.0);
}
