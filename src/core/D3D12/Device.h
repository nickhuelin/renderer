#ifndef DEVICE_H
#define DEVICE_H

#include "d3d12Common.h"
#include "SwapChain.h"

constexpr UINT32 FrameBufferCount = 3;

#endif