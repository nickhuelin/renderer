#include "GLSLShader.h"

void GLSLShader::Activate()
{
	glUseProgram(m_Program);
}

void GLSLShader::Deactivate()
{
	glUseProgram(0);
}

void GLSLShader::Delete()
{
	glDeleteProgram(m_Program);
	m_Program = 0;

	if (vertexCode) {
		free((void*)vertexCode);
		vertexCode = nullptr;
	}
	if (fragmentCode) {
		free((void*)fragmentCode);
		fragmentCode = nullptr;
	}
}

GLSLShader::GLSLShader()
{
	m_Program = 0;
	vertexCode = nullptr;
	fragmentCode = nullptr;
}

GLSLShader::~GLSLShader()
{
	Delete();
}

void GLSLShader::LoadFromFile(const char* vertexPath, const char* fragmentPath)
{
	// Load from text files as fstream
	std::fstream vShaderFile, fShaderFile;

	// is path valid?
	if (!vertexPath || !fragmentPath) {
		LOG_ERROR("ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ");
		return;
	}
	

	try {
		// set to readonly mode as text
		vShaderFile.open(vertexPath, std::ios::in);
		fShaderFile.open(fragmentPath, std::ios::in);

		std::stringstream vShaderStream, fShaderStream;

		// read whole file into buffer
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();

		// close file handlers
		vShaderFile.close();
		fShaderFile.close();

		// put in vertexCode and fragmentCode
		vertexCode = _strdup(vShaderStream.str().c_str());
		fragmentCode = _strdup(fShaderStream.str().c_str());
	}
	catch (const std::ifstream::failure& e) {
		// log error handle e
	LOG_ERROR("ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ\n%s", e.what());
	}
}


GLint GLSLShader::CreateAndLinkProgram()
{
	// create and link shader program
 	m_Program = glCreateProgram();

	// create shader objects
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// attach shaders to program
	glAttachShader(m_Program, vertexShader);
	glAttachShader(m_Program, fragmentShader);

	// compile shaders
	glShaderSource(vertexShader, 1, &vertexCode, NULL);
	glShaderSource(fragmentShader, 1, &fragmentCode, NULL);

	glCompileShader(vertexShader);
	// compile fragment shader
	glCompileShader(fragmentShader);
	// link shaders
	glLinkProgram(m_Program);

	// delete shaders
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// return success
	return 0;
}

void GLSLShader::SetUniform(const char* name, float x, float y, float z)
{
	GLint loc = glGetUniformLocation(m_Program, name);
	glUniform3f(loc, x, y, z);
}

void GLSLShader::SetUniform(const char* name, const Vector2& v)
{
	GLint loc = glGetUniformLocation(m_Program, name);
	glUniform2f(loc, v.x, v.y);
}

void GLSLShader::SetUniform(const char* name, const Vector3& v)
{
	GLint loc = glGetUniformLocation(m_Program, name);
	glUniform3f(loc, v.x, v.y, v.z);
}

void GLSLShader::SetUniform(const char* name, const Vector4& v)
{
	GLint loc = glGetUniformLocation(m_Program, name);
	glUniform4f(loc, v.x, v.y, v.z, v.w);
}

GLuint GLSLShader::GetProgram() const
{
	return m_Program;
}

GLuint GLSLShader::GetAttributeLocation(const char* name) const
{
	return glGetAttribLocation(m_Program, name);
}

GLuint GLSLShader::GetUniformLocation(const char* name) const
{
	return glGetUniformLocation(m_Program, name);
}
