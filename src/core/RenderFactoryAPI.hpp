#ifndef RENDER_FACTORY_API_H
#define RENDER_FACTORY_API_H

#include "Renderer.hpp"
#include "RendererOpenGL.hpp"
#include "RenderDX12.h"

class RendererAPIFactory {
public:
	static Renderer* CreateRenderer(RenderAPIType type, HWND window) {
		switch (type) {
		case RenderAPIType::OpenGL:
			return new RendererOpenGL(window);
		case RenderAPIType::DirectX12:
			return new RendererDX12();
		default:
			return nullptr;
		}
	}
	// reference return

};
#endif // !RENDER_FACTORY_API_H
