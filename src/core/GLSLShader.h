#ifndef GLSLSHADER_H
#define GLSLSHADER_H

#include "Shader.h"
#include "GFXMath.h"
#include "glad/include/glad/glad.h"
#include "Logger.hpp"

class GLSLShader : public Shader
{
public:
	GLSLShader();
	~GLSLShader();

	void Activate() override;
	void Deactivate() override;
	void Delete() override;

	void LoadFromFile(const char* vertexPath, const char* fragmentPath) override;

	GLint CreateAndLinkProgram();

	// set uniform functions
	void SetUniform(const char* name, float x, float y, float z);
	void SetUniform(const char* name, const Vector2& v);
	void SetUniform(const char* name, const Vector3& v);
	void SetUniform(const char* name, const Vector4& v);

	// get variable values from shader
	GLuint GetProgram() const;
	GLuint GetAttributeLocation(const char* name) const;
	GLuint GetUniformLocation(const char* name) const;

private:
	// program
	GLuint m_Program = 0;
	const char* vertexCode = nullptr;
	const char* fragmentCode = nullptr;
};

#endif
